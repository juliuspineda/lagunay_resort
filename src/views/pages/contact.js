import React, { Fragment } from 'react'

export default function ContactUsPage() {
	return (
		<Fragment>
			<div class="bradcam_area breadcam_bg_2">
		        <h3>Get in Touch</h3>
		    </div>

			<section class="contact-section" style={{ backgroundColor: "#e8e4c9"}}>
	            <div class="container">
	    			<div>
	                <iframe 
		                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.65930777308!2d121.45336071450676!3d14.561464981939132!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33978da27d3f34f1%3A0x96567b050e9b7f81!2sLagunay%20Resort!5e0!3m2!1sen!2sph!4v1581858224061!5m2!1sen!2sph" 
		                width="1100" 
		                height="600" 
		                frameborder="0" 
		                styles="border:0;" 
		                allowfullscreen=""
	                />
	    			</div>

	                <div class="row">
	                    <div class="col-12">
	                        <h2 class="contact-title">Get in Touch</h2>
	                    </div>
	                    <div class="col-lg-8">
	                        <form class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
	                            <div class="row">
	                                <div class="col-12">
	                                    <div class="form-group">
	                                        <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Message'" placeholder=" Message"></textarea>
	                                    </div>
	                                </div>
	                                <div class="col-sm-6">
	                                    <div class="form-group">
	                                        <input class="form-control valid" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" placeholder="Enter your name"/>
	                                    </div>
	                                </div>
	                                <div class="col-sm-6">
	                                    <div class="form-group">
	                                        <input class="form-control valid" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" placeholder="Email"/>
	                                    </div>
	                                </div>
	                                <div class="col-12">
	                                    <div class="form-group">
	                                        <input class="form-control" name="subject" id="subject" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Subject'" placeholder="Enter Subject"/>
	                                    </div>
	                                </div>
	                            </div>
	                            <div class="form-group mt-3">
	                                <button type="submit" class="button button-contactForm boxed-btn">Send</button>
	                            </div>
	                        </form>
	                    </div>
	                    <div class="col-lg-3 offset-lg-1">
	                        <div class="media contact-info">
	                            <span class="contact-info__icon"><i class="ti-home"></i></span>
	                            <div class="media-body">
	                                <h3>Buttonwood, California.</h3>
	                                <p>Rosemead, CA 91770</p>
	                            </div>
	                        </div>
	                        <div class="media contact-info">
	                            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
	                            <div class="media-body">
	                                <h3>+1 253 565 2365</h3>
	                                <p>Mon to Fri 9am to 6pm</p>
	                            </div>
	                        </div>
	                        <div class="media contact-info">
	                            <span class="contact-info__icon"><i class="ti-email"></i></span>
	                            <div class="media-body">
	                                <h3>support@colorlib.com</h3>
	                                <p>Send us your query anytime!</p>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	    </section>
		</Fragment>
	)
}