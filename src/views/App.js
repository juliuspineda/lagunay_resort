import React, {Fragment} from 'react';
import { Switch, Route } from 'react-router-dom';

// Components
import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import Booking from './components/booking/BookingAvailables';
import BookReceipt from './components/booking/BookReceipt';
import BookTransaction from './components/booking/BookTransaction';

// Pages
import Home from './pages/home';
import Rooms from './pages/rooms';
import About from './pages/about';
import Blog from './pages/blog';
import SingleBlog from './pages/singleBlog';
import ContactUs from './pages/contact';

function App() {
    return(
        <Fragment>
            <Header/>
            <Switch>
                <Route exact path={ '/' } component={ Home }/>
                <Route path={ '/home' } component={ Home }/>
                <Route path={ '/rooms' } component={ Rooms }/>
                <Route path={ '/about' } component={ About }/>
                <Route path={ '/blog' } component={ Blog }/>
                <Route path={ '/single-blog' } component={ SingleBlog }/>
                <Route path={ '/contact-us' } component={ ContactUs }/>
                <Route path={ '/booking' } component={ Booking }/>
                <Route path={ '/resorts' } component={ BookReceipt }/>
                <Route path={ '/transactions' } component={ BookTransaction }/>
            </Switch>
            <Footer/>
        </Fragment>
    )
}

export default App;