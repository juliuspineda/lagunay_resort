import React, {Fragment,Component} from 'react';
import 'react-dates/initialize';
import { DateRangePicker, DayPickerSingleDateController } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import StarIcon from '@material-ui/icons/Star';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import WifiIcon from '@material-ui/icons/Wifi';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import LocalLaundryServiceIcon from '@material-ui/icons/LocalLaundryService';
import AcUnitIcon from '@material-ui/icons/AcUnit';
import FreeBreakfastIcon from '@material-ui/icons/FreeBreakfast';
import { Link } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';

import Slider from "react-slick";

// import { makeStyles } from '@material-ui/core/styles'; 

// const useStyles = makeStyles({
//     // root: {
//     //     backgroundColor: 'red',
//     //     color: props => props.color,
//     // },
//     DateRangePickerInput: {
//         display: 'flex'
//     }
// });

const getData = () => {
    return { 
        title: 'Villa Resort', 
        location: 'Quezon City',
        amount: 5000, 
        guests: 10, 
        bedrooms: 4,
        days: 2, 
        baths: 2,
        status: 'night',
        description: 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance.',
        amenities: [{
            name: 'wifi'
        },
        {
            name: 'wifi'
        }],
    }
} 

const availableRooms = () => {
    return [
        {
            title: 'Villa Resort 1',
            description: 'lorem lorem lorem lorem lorem ',
            price: 'P2,000',
            imgURL: 'img/resorts/img-1.jpg',
            status: 'night'
        },
        {
            title: 'Villa Resort 2',
            description: 'lorem lorem lorem lorem lorem ',
            price: 'P5,000',
            imgURL: 'img/resorts/img-2.jpg',
            status: 'day'
        },
        {
            title: 'Villa Resort 3',
            description: 'lorem lorem lorem lorem lorem ',
            price: 'P1,000',
            imgURL: 'img/resorts/img-3.jpg',
            status: 'day'
        },
        {
            title: 'Villa Resort 4',
            description: 'lorem lorem lorem lorem lorem ',
            price: 'P7,000',
            imgURL: 'img/resorts/img-4.jpg',
            status: 'night'
        },
        {
            title: 'Villa Resort 5',
            description: 'lorem lorem lorem lorem lorem ',
            price: 'P10,000',
            imgURL: 'img/resorts/img-5.jpg',
            status: 'night'
        }
    ]
}

class BookReceipt extends Component {
    constructor(props) {
        super(props)

        this.state = {
            startDate: null,
            endDate: null
        }
    }
    render() {
    // const classes = useStyles()
    const {startDate,endDate} = this.state
    const bookData = getData()
    const availableResorts = availableRooms()

    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
      };

    return(
        <Fragment>
            <div className="bradcam_area breadcam_bg">
		    </div>
            <div className="menu_area">
                <div className="container">
                    <div id="book-menu" className="row">
                        <div className="col-xl-8 col-lg-8">
                            <div>
                                <h1>{bookData.title}</h1>
                            </div>
                            <div className="mt-2">
                                <h4>{bookData.location}</h4>
                            </div>
                            <div className="d-flex mt-3 mb-4">
                                <div className="mr-1 category-container cursor-pt">
                                    <p className="category-sub">{(bookData.guests > 1) ? bookData.guests + ' guests' : bookData.guests + ' guest' }</p>
                                </div>
                                <div className="mr-1 category-container cursor-pt">
                                    <p className="category-sub">{(bookData.bedrooms > 1) ? bookData.bedrooms + ' bedrooms' : bookData.bedrooms + ' bedroom'}</p>
                                </div>
                                <div className="mr-1 category-container cursor-pt">
                                    <p className="category-sub">{(bookData.baths > 1) ? bookData.baths + ' baths' : bookData.baths + ' bath'}</p>
                                </div>        
                            </div>
                            <div>
                            <Slider {...settings}>
                                {
                                    availableResorts && availableResorts.map((data, key) => {
                                        return <div key={key}>
                                            <img style={{ objectFit: 'cover' }} width="100%" height="250px" src={data.imgURL} alt="..."/>
                                        </div>
                                    })
                                }
                            </Slider>
                            </div>
                            <div className="mt-5">
                                <p>{bookData.description}</p>
                                <div className="read-more">
                                    <p className="read-more-p">Read more</p><ExpandMoreIcon style={{ width: '20px' }}/>
                                </div>
                            </div>
                            <hr/>
                            <div>
                                <h4 className="mt-4">Amenities</h4>
                                <div className="d-flex mt-4">
                                    <div className="row w-100">
                                        {/* {
                                            bookData && bookData.amenities.map((data,key) => {
                                            return <div className="col-xl-6 col-lg-6">
                                                    <div className="d-flex">
                                                        <WifiIcon/>
                                                        <p className="pl-2">
                                                            {data.name}
                                                        </p>
                                                    </div>
                                                </div>
                                            })
                                        } */}
                                        <div className="col-xl-6 col-lg-6">
                                            <div className="d-flex">
                                                <WifiIcon className="icon-shades-gray"/>
                                                <p className="pl-2">
                                                    Wifi
                                                </p>
                                            </div>
                                        </div>
                                        <div className="col-xl-6 col-lg-6">
                                            <div className="d-flex">
                                                <LocalLaundryServiceIcon className="icon-shades-gray"/>
                                                <p className="pl-2">
                                                    Dryer
                                                </p>
                                            </div>
                                        </div>
                                        <div className="col-xl-6 col-lg-6">
                                            <div className="d-flex">
                                                <AcUnitIcon className="icon-shades-gray"/>
                                                <p className="pl-2">
                                                    Air Conditioning
                                                </p>
                                            </div>
                                        </div>
                                        <div className="col-xl-6 col-lg-6">
                                            <div className="d-flex">
                                                <FreeBreakfastIcon className="icon-shades-gray"/>
                                                <p className="pl-2">
                                                    Free Coffee
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>      
                            
                            <hr/>
                            <div className="mb-2">
                                <h2 className="mb-4">Reviews</h2>
                            </div>
                            <div className="d-flex mb-3">
                                <div className="mr-4">
                                    <img className="review-icon" width="50px" height="auto" src={availableResorts[0].imgURL}/>
                                </div>
                                <div className="align-self-center">
                                    <div className="mr-2 review-title-name">
                                        Fred
                                    </div>
                                    <div className="review-sub-title-name">
                                        February 2020
                                    </div>
                                </div>
                            </div>
                            <div className="d-flex">
                                <p>{bookData.description}</p>
                            </div>
                            <hr/>
                            <div className="d-flex mb-3">
                                <div className="mr-4">
                                    <img className="review-icon" width="50px" height="auto" src={availableResorts[0].imgURL}/>
                                </div>
                                <div className="align-self-center">
                                    <div className="mr-2 review-title-name">
                                        Fred
                                    </div>
                                    <div className="review-sub-title-name">
                                        February 2020
                                    </div>
                                </div>
                            </div>
                            <div className="d-flex mb-4">
                                <p>{bookData.description}</p>
                            </div>
                            <Pagination count={10} variant="outlined" shape="rounded" />
                           
                            <hr/>
                            <div>
                                <h2>Location</h2>
                                <p>you can locate this resort..</p>
                            <iframe 
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.65930777308!2d121.45336071450676!3d14.561464981939132!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33978da27d3f34f1%3A0x96567b050e9b7f81!2sLagunay%20Resort!5e0!3m2!1sen!2sph!4v1581858224061!5m2!1sen!2sph" 
                                width="100%" 
                                height="300px" 
                                frameborder="0" 
                                styles="border:0;" 
                                allowfullscreen=""
                            />
                            </div>
                        </div>
                        <div id="book-col-1" className="col-xl-4 col-lg-4">
                            <div className="br-1 p-3 w-100">
                                <div>
                                    <h3>&#8369;5,000 /<span style={{ margin: '0px', fontSize: '12px',fontWeight: '700' }}>per night</span></h3>
                                    <div className="d-inline-flex">
                                        <StarIcon
                                            style={{ width: '15px', height: '15px', color: '#f2ff25' }}
                                        />
                                        <StarIcon
                                            style={{ width: '15px', height: '15px', color: '#f2ff25' }}
                                        />
                                        <StarIcon
                                            style={{ width: '15px', height: '15px', color: '#f2ff25' }}
                                        />
                                        <StarIcon
                                            style={{ width: '15px', height: '15px', color: '#f2ff25' }}
                                        />
                                        <StarIcon
                                            style={{ width: '15px', height: '15px', color: '#f2ff25' }}
                                        />
                                    </div>
                                </div>
                                <hr/>
                                <DateRangePicker
                                    startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                                    startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
                                    endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                                    endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
                                    onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })} // PropTypes.func.isRequired,
                                    focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                                    onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                                    />
                                <form>
                                    <div>
                                        <div className="mb-3">
                                            <label>Guests</label>
                                            <select class="form-select wide" id="default-select" className="w-100">
                                                <option data-display="1 Guest">1 Guest</option>
                                                <option value="1">2 Guest</option>
                                                <option value="2">3 Guest</option>
                                            </select>
                                        </div>
                                        <div className="mb-3 d-inline-flex w-100 mt-4">
                                            <div className="w-50">
                                                <p className="size-description m-0">&#8369;{bookData.amount} x { bookData.days + ' ' + bookData.status}</p>
                                            </div>  
                                            <div className="w-50">
                                                <p className="text-right size-description m-0">&#8369;{bookData.amount * 2}</p>
                                            </div>
                                        </div>
                                        <hr style={{ margin: '-10px 0px' }}/>
                                        <div className="mb-3 d-inline-flex w-100 mt-3">
                                            <div className="w-50">
                                                <p className="size-description m-0">Service fee</p>
                                            </div>  
                                            <div className="w-50">
                                                <p className="text-right size-description m-0">&#8369;500</p>
                                            </div>
                                        </div>
                                        <hr style={{ margin: '-10px 0px' }}/>
                                        <div className="mb-3 d-inline-flex w-100 mt-3">
                                            <div className="w-50">
                                                <p className="size-description m-0" style={{ fontWeight: '500' }}>Total</p>
                                            </div>  
                                            <div className="w-50">
                                                <p className="text-right size-description m-0">&#8369;{(bookData.amount * 2) + 500}</p>
                                            </div>
                                        </div>
                                        <div style={{ marginTop: '5px' }}>
                                            <a href={ '/transactions' } class="reserve_now w-100 text-center">Reserve Now</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="mt-5 mb-2">
                        <h2>More homes you may like</h2>
                    </div>
                    <div className="d-flex">
                        <div className="row">
                            {
                                availableResorts && availableResorts.map((data, index) => {
                                    return (
                                    <div className="col-xl-4 col-lg-4 mb-3 booking-section">
                                    <div className="mb-3 of-hidden" onClick={ () => console.log('success') }>
                                        <a href="/resorts">
                                            <img height={ '180px' } src={data.imgURL} alt="..."/>
                                        </a>
                                    </div>
                                        <div>
                                            <div className="d-flex">
                                                <div className="flex-1">
                                                    <h4>{data.title}</h4>
                                                </div>
                                                <div className="flex-1 text-right">
                                                </div>
                                            </div>
                                            <p style={{ fontSize: '12px' }}>{data.description}<br/>
                                            <span style={{ fontSize: '18px', fontWeight: '600' }}>{data.price}</span> / <span style={{ fontSize: '18px' }}>{data.status}</span></p>
                                            <a href="/transactions" class="book_now w-100 text-center">book now</a>
                                        </div>
                                    </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
        )
    }
}

export default BookReceipt;