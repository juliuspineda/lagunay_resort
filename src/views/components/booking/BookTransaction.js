import React, {Fragment,Component} from 'react';
import { Card } from '@material-ui/core'

const COLOR_REQUIRED = 'red';

class BookTransaction extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return(
            <Fragment>
                <div className="bradcam_area breadcam_bg"></div>
                <div className="menu_area">
                    <div className="container">
                        <div className="row">
                            <div className="col-xl-8 col-lg-8">
                                <div>
                                    <h1>Add your Informations</h1>
                                    <p>please fill-up the ff:</p>
                                </div>
                                <div className="row mt-3">
                                    <div className="col-xl-6 col-lg-6">
                                        <div>
                                            <label>First Name <span style={{ color: COLOR_REQUIRED }}>*</span></label>
                                        </div>
                                        <div className="contact-info-input">
                                            <input className="contact-input" placeholder="" />
                                        </div>
                                    </div>
                                    <div className="col-xl-6 col-lg-6">
                                        <div>
                                            <label>Last Name <span style={{ color: COLOR_REQUIRED }}>*</span></label>
                                        </div>
                                        <div className="contact-info-input">
                                            <input className="contact-input" placeholder="" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xl-6 col-lg-6">
                                        <div>
                                            <label>Email <span style={{ color: COLOR_REQUIRED }}>*</span></label>
                                        </div>
                                        <div className="contact-info-input">
                                            <input className="contact-input" placeholder="" />
                                        </div>
                                    </div>
                                    <div className="col-xl-6 col-lg-6">
                                        <div>
                                            <label>Telephone <span style={{ color: COLOR_REQUIRED }}>*</span></label>
                                        </div>
                                        <div className="contact-info-input">
                                            <input className="contact-input" placeholder="" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xl-6 col-lg-6">
                                        <div>
                                            <label>Address <span style={{ color: COLOR_REQUIRED }}>*</span></label>
                                        </div>
                                        <div className="contact-info-input">
                                            <input className="contact-input" placeholder="" />
                                        </div>
                                    </div>
                                    <div className="col-xl-6 col-lg-6">
                                        <div>
                                            <label>City <span style={{ color: COLOR_REQUIRED }}>*</span></label>
                                        </div>
                                        <div className="contact-info-input">
                                            <input className="contact-input" placeholder="" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xl-6 col-lg-6">
                                        <div>
                                            <label>Country <span style={{ color: COLOR_REQUIRED }}>*</span></label>
                                        </div>
                                        <div className="contact-info-input">
                                            <input className="contact-input" placeholder="" />
                                        </div>
                                    </div>
                                    <div className="col-xl-6 col-lg-6">
                                        <div>
                                            <label>ZIP <span style={{ color: COLOR_REQUIRED }}>*</span></label>
                                        </div>
                                        <div className="contact-info-input">
                                            <input className="contact-input" placeholder="" />
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xl-12 col-lg-12">
                                        <div>
                                            <label>Request <span style={{ color: COLOR_REQUIRED }}>*</span></label>
                                        </div>
                                        <div className="contact-info-input">
                                            <textarea className="contact-input" placeholder="" />
                                        </div>
                                        <div>
                                            <label>Arrival <span style={{ color: COLOR_REQUIRED }}>*</span></label>
                                        </div>
                                        <div className="contact-info-input">
                                            <select className="contact-select">
                                                <option>asd</option>
                                                <option>asd</option>
                                                <option>asd</option>
                                            </select>
                                        </div>
                                        <div style={{ marginTop: '3.5rem' }}>
                                            <label>Coupon <span style={{ color: COLOR_REQUIRED }}>*</span></label>
                                            <div className="contact-info-input">
                                                <input className="contact-input" placeholder="" />
                                            </div>
                                        </div>
                                        <div>
                                            <div className="d-flex mt-3 align-items-center">
                                                <input style={{ transform: 'scale(1.5)', marginRight: '1rem', marginBottom: '.5rem' }} type="checkbox"/>
                                                <label>Terms and conditions <span style={{ color: COLOR_REQUIRED }}>*</span></label>
                                            </div>
                                            <div className="contact-info-input mt-3">
                                                <a style={{ fontSize: '20px' }} href="#" class="contact_now w-100 text-center">Check Out</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-4">
                                <div className="position-relative">
                                    <Card style={{ padding: '1.5rem', marginBottom: '2rem' }}>
                                        <div className="text-center">
                                            <h4 className="mb-4" style={{ fontSize: '30px' }}>Your Reservation</h4>
                                            <hr />
                                            <div className="row mb-4">
                                                <div className="col-xl-6 col-lg-6">
                                                    <div style={{ background: '#009DFF', padding: '15px', borderRadius: '10px' }}>
                                                        <p className="color-white">Check-In</p>
                                                        <p className="color-white font-size-5 font-weight-500">22</p>
                                                        <p className="color-white mt-4">Feb, 2020</p>
                                                        <p className="color-white font-style-cards mb-0">Saturday</p>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6">
                                                    <div style={{ background: '#009DFF', padding: '15px', borderRadius: '10px' }}>
                                                        <p className="color-white">Check-Out</p>
                                                        <p className="color-white font-size-5 font-weight-500">22</p>
                                                        <p className="color-white mt-4">Feb, 2020</p>
                                                        <p className="color-white font-style-cards mb-0">Saturday</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row mb-4">
                                                <div className="col-xl-6 col-lg-6">
                                                    <div style={{ background: '#009DFF', padding: '20px 15px 15px 15px', borderRadius: '10px' }}>
                                                        <p className="color-white font-size-5 font-weight-500">1</p>
                                                        <p className="color-white mb-0" style={{ marginTop: '1.5rem', fontSize: '20px', fontWeight: '500' }}>Guests</p>
                                                    </div>
                                                </div>
                                                <div className="col-xl-6 col-lg-6">
                                                    <div style={{ background: '#009DFF', padding: '20px 15px 15px 15px', borderRadius: '10px' }}>
                                                        <p className="color-white font-size-5 font-weight-500">1</p>
                                                        <p className="color-white mb-0" style={{ marginTop: '1.5rem', fontSize: '20px', fontWeight: '500' }}>Nights</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="row mb-4">
                                                <div className="col-xl-12 col-lg-12 w-100">
                                                    <div style={{ background: '#009DFF', padding: '20px 15px 15px 15px', borderRadius: '10px' }}>
                                                        <p className="color-white font-weight-500" style={{ fontSize: '24px' }}>30 % Discount</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Card>
                                    <div className="position-relative">
                                        <div className="">
                                            <img style={{ opacity: '.8' }} width="100%" height="200px" src={ 'img/resorts/img-1.jpg' }/>
                                        </div>
                                        <div className="text-index">
                                            <p className="text-index-style">Villa Resort</p>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default BookTransaction;