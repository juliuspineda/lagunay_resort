import React, {Fragment} from 'react';
import '../../../styles/main.css';
import StarIcon from '@material-ui/icons/Star'
import { Link } from '@material-ui/core';

const availableRooms = () => {
    return [
        {
            title: 'Villa Resort 1',
            description: 'lorem lorem lorem lorem lorem ',
            price: 'P2,000',
            imgURL: 'img/resorts/img-1.jpg',
            status: 'night'
        },
        {
            title: 'Villa Resort 2',
            description: 'lorem lorem lorem lorem lorem ',
            price: 'P5,000',
            imgURL: 'img/resorts/img-2.jpg',
            status: 'day'
        },
        {
            title: 'Villa Resort 3',
            description: 'lorem lorem lorem lorem lorem ',
            price: 'P1,000',
            imgURL: 'img/resorts/img-3.jpg',
            status: 'day'
        },
        {
            title: 'Villa Resort 4',
            description: 'lorem lorem lorem lorem lorem ',
            price: 'P7,000',
            imgURL: 'img/resorts/img-4.jpg',
            status: 'night'
        },
        {
            title: 'Villa Resort 5',
            description: 'lorem lorem lorem lorem lorem ',
            price: 'P10,000',
            imgURL: 'img/resorts/img-5.jpg',
            status: 'night'
        }
    ]
}

const BookingAvailables = () => {
    const availableResorts = availableRooms()
    return(
        <Fragment>
			<div className="bradcam_area breadcam_bg">
		    </div>
		    <div className="menu_area">
		        <div className="container">
		            <div className="row">
		                <div className="col-xl-3 col-lg-3">
		                    <div className="about_info">
		                        <div className="section_title mb-20px">
		                            <span>/Booking</span>
		                            <h4>Available Villas</h4>
		                        </div>
		                        <p>Suscipit libero pretium nullam potenti. Interdum</p>
		                        <a href="#" className="line-button">Learn More</a>
		                    </div>
                            <div className="d-flex">
                                <div className="br-1 p-3 w-100 mt-5">
                                    <h4>Search Filter</h4>
                                    <form>
                                        <div className="mb-5 mt-4">
                                            <label>Standard</label>
                                            <select class="nice-select wide" className="w-100">
                                                <option data-display="Villa Resort">Villa Resort</option>
                                                <option value="1">Villa Resort</option>
                                                <option value="2">Villa Resort</option>
                                            </select>
                                        </div>
                                        <div className="mb-5 mt-5">  
                                            <label>Lowest to Highest</label>
                                            <select class="nice-select wide" className="w-100">
                                                <option data-display="P1,000 - P2,000">P1,000 - P2,000</option>
                                                <option value="1">P3,000 - P4,000</option>
                                                <option value="2">P5,000 - P6,000</option>
                                                <option value="3">P7,000 - P8,000</option>
                                                <option value="4">P9,000 - Up</option>
                                            </select>
                                        </div>
                                        <div className="mb-5 mt-5">
                                            <label>Highest to Lowest</label>
                                            <select class="nice-select wide" className="w-100">
                                                <option data-display="P10,000 - P9,000">P10,000 - P9,000</option>
                                                <option value="1">P3,000 - P4,000</option>
                                                <option value="2">P5,000 - P6,000</option>
                                                <option value="3">P7,000 - P8,000</option>
                                                <option value="4">P9,000 - Down</option>
                                            </select>
                                        </div>
                                        <div className="mt-4">
                                            <a href={ '#' } class="reserve_now w-100 text-center">Search</a>
                                        </div>
                                    </form>
                                </div>
                            </div>
		                </div>
		                <div className="col-xl-9 col-lg-9">
                            <div className="d-flex mb-3">
                                <div className="col-xl-8 col-lg-8" style={{ marginLeft: '-1rem' }}>
                                    <form action="">
                                        <div style={{ border: '2px solid #dee2e6' }} class="input-group mb-4 rounded-pill p-1 text-right">
                                            <input type="search" placeholder="Search..." aria-describedby="button-addon3" class="form-control bg-none border-0"/>
                                            <div class="input-group-append border-0">
                                            <button id="button-addon3" type="button" class="btn btn-link search-icon-dark"><i class="fa fa-search"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div className="d-flex">
                                <div className="row">
                                    {
                                        availableResorts && availableResorts.map((data, index) => {
                                            return (
                                            <div className="col-xl-4 col-lg-4 mb-3 booking-section">
                                            <div className="mb-3 of-hidden" onClick={ () => console.log('success') }>
                                                <a href="/resorts">
                                                    <img height={ '180px' } src={data.imgURL} alt="..."/>
                                                </a>
                                            </div>
                                                <div>
                                                    <div className="d-flex">
                                                        <div className="flex-1">
                                                            <h4>{data.title}</h4>
                                                        </div>
                                                        <div className="flex-1 text-right">
                                                            {/*<StarIcon
                                                                style={{ width: '20px', height: '20px', color: '#f2ff25' }}
                                                            />
                                                            <StarIcon
                                                                style={{ width: '20px', height: '20px', color: '#f2ff25' }}
                                                            />
                                                            <StarIcon
                                                                style={{ width: '20px', height: '20px', color: '#f2ff25' }}
                                                            />
                                                            <StarIcon
                                                                style={{ width: '20px', height: '20px', color: '#f2ff25' }}
                                                            />
                                                            <StarIcon
                                                                style={{ width: '20px', height: '20px', color: '#f2ff25' }}
                                                            />*/}
                                                        </div>
                                                    </div>
                                                    <p style={{ fontSize: '12px' }}>{data.description}<br/>
                                                    <span style={{ fontSize: '18px', fontWeight: '600' }}>{data.price}</span> / <span style={{ fontSize: '18px' }}>{data.status}</span></p>
                                                    <a href={ '/transactions' } class="book_now w-100 text-center">book now</a>
                                                </div>
                                            </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</Fragment>
    )
}

export default BookingAvailables;