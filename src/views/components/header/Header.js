import React, { Fragment, useState } from 'react';
import BookModal from '../booking/BookModal';
import { Link } from 'react-router-dom';

const pathArrays = () => {
    return [
        { url: "/", status: "active", name: "Home" },
        { url: "/rooms", status: "active", name: "Rooms" },
        { url: "/about", status: "active", name: "About" },
        { url: "/blog", status: "active", name: "Blog" },
        { url: "/contact-us", status: "active", name: "Contact" }]
}

const pathSocials = () => {
    return [
        { url: "#", class: "fa fa-facebook-square" },
        { url: "#", class: "fa fa-twitter" },
        { url: "#", class: "fa fa-instagram" },
    ]
}

const Header = () => {

    const pathDataArrays = pathArrays()
    const pathDataSocials = pathSocials()

    return(
        <Fragment>
            <header>
                <div className="header-area">
                    <div id="sticky-header" className="main-header-area">
                        <div className="container-fluid p-0">
                            <div className="row align-items-center no-gutters">
                                <div className="col-xl-5 col-lg-6">
                                    <div className="main-menu  d-none d-lg-block">
                                            <nav>
                                                <ul id="navigation">
                                                    {
                                                        pathDataArrays && pathDataArrays.map((data, i) => {
                                                            return (
                                                                <li>
                                                                <Link className={ window.location.pathname === data.url ? data.status : null } key={i} to={data.url}>
                                                                    { data.name }
                                                                </Link>
                                                            </li>
                                                            )
                                                        })
                                                    }
                                                </ul>
                                            </nav>
                                    </div>
                                </div>
                                <div className="col-xl-2 col-lg-2">
                                    <div className="logo-img">
                                        <a href="index.html">
                                            <img src="img/logo.png" alt=""/>
                                        </a>
                                    </div>
                                </div>
                                <div className="col-xl-5 col-lg-4 d-none d-lg-block">
                                    <div className="book_room">
                                        <div className="socail_links">
                                            <ul>
                                                {
                                                    pathDataSocials && pathDataSocials.map((data, i) => {
                                                        return (
                                                            <li>
                                                                <Link to={data.url}>
                                                                    <i className={data.class}></i>
                                                                </Link>
                                                            </li>
                                                        )
                                                    })
                                                }
                                            </ul>
                                        </div>
                                        <div 
                                            className="book_btn d-none d-lg-block"
                                        >
                                            <a className="popup-with-form" href={ "#test-form" }>Book Now</a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12">
                                    <div className="mobile_menu d-block d-lg-none"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <BookModal />
        </Fragment>
    )
}

export default Header;